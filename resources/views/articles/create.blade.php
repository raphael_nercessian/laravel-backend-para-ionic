@extends('layout')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Novo artigo</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('articles.index') }}"> Voltar</a>
            </div>
        </div>
    </div

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Erro!</strong> Verifique os campos.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
 <form class="form-horizontal" action="{{ route('articles.store') }}" method="post">
    {!! Form::open(array('route' => 'articles.store','method'=>'POST')) !!}
         @include('articles.form')
    {!! Form::close() !!}

@endsection